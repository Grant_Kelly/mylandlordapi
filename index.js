// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var tediousExpress = require('express4-tedious');
var tediousConnection = require('tedious').Connection;
var tediousRequest = require('tedious').Request;

var connection = {
    "server"  : "my-landlord.database.windows.net",
    "userName": "admin1",
    "password": "Password123",
    "options": { "encrypt": true, "database": "my-landlord-db" }
};

app.use(function (req, res, next) {

    req.sql = tediousExpress(connection);

    next();

});

function queryDatabase(query, callback){
    var ted_conn = new tediousConnection(connection);
    var newdata = [];
    var dataset = [];
    ted_conn.on('connect', function(err) {

        var sql = query;

        var Request = tediousRequest;
        var request = new Request(sql, function (err, rowCount) {
            if (err) {
                callback(err, false, ted_conn);
            } else {
                if (rowCount < 1) {
                    callback(null, false, ted_conn);
                } else {
                    callback(null, newdata, ted_conn);
                }
            }
        });

        request.on('row', function(columns) {

            columns.forEach(function(column) {
                   dataset.push({
                       col: column.metadata.colName,
                       val: column.value
                   });


            });

            newdata.push(dataset);

        });

        ted_conn.execSql(request);
        //ted_conn.close();

    });

    //ted_conn.close();

}


function generateRandomNumber(min_value , max_value) 
{
    return Math.floor((Math.random() * max_value) + min_value);
} 


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 1337;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api! You sent a GET!' });   
});
router.post('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api! You sent a POST!' });
    console.log(req.body.Username)
    console.log(req.body.Email)
    console.log(req.body.Password)

});
router.get('/tenants', function (req, res) {

    req.sql("select * from dbo.tenant for json path")
        .into(res);

});

router.post('/isTenant', function (req, res) {

    uname = req.body.username;
    tenantStr = "select tenant_id from dbo.tenant where username=\'" + uname + "\'";
    tenant = false;

    //check if tenant
    queryDatabase(tenantStr, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            ted_conn.close();
            res.sendStatus(200);
        }else{
            ted_conn.close();
            res.sendStatus(404);
        }
    });
});

router.post('/isLandlord', function (req, res) {

    uname = req.body.username;
    landlordStr = "select landlord_id from dbo.landlord where username=\'" + uname + "\'";
    landlord = false;

    //check if tenant
    queryDatabase(landlordStr, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            ted_conn.close();
            res.sendStatus(200);
        }else{
            ted_conn.close();
            res.sendStatus(404);
        }
    });
});

router.post('/tenantid', function (req, res) {

    uname = req.body.username;
    tenantStr = "select tenant_id from dbo.tenant where username=\'" + uname + "\'";
    tenant = false;

    //check if tenant
    queryDatabase(tenantStr, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            ted_conn.close();
            res.send(rows);
        }else{
            ted_conn.close();
            res.sendStatus(404);
        }
    });
});

router.post('/landlordid', function (req, res) {

    tenantid = req.body.tenantid;
    landlordStr = "select landlord_id from dbo.landlord_to_tenant where tenant_id='" + tenantid + "'";
    landlord = false;

    //check if tenant
    queryDatabase(landlordStr, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            ted_conn.close();
            res.send(rows);
        }else{
            ted_conn.close();
            res.sendStatus(404);
        }
    });
});

router.post('/maintreq', function (req, res) {
    landlordid = req.body.landlordid;
    tenantid = req.body.tenantid;
    room = req.body.room;
    condition = req.body.condition;
    story = req.body.story;

    maintreqStr = "insert into dbo.maintenance_request (landlord_id, tenant_id, story, room, condition) values ("+
    landlordid+", "+tenantid+", "+story+", "+room+", "+condition+")";
    queryDatabase(maintreqStr, function(err, rows, ted_conn) {
        if (err) {
            ted_conn.close();
            res.sendStatus(404);
        } else {
            ted_conn.close();
            res.sendStatus(200);
        }
    });
});

router.post('/login', function (req, res) {

    uname = req.body.username;
    pwrd = req.body.password;
    uname_match = false;
    pwrd_match = false;
    str = 'select * from dbo.tenant';

    queryDatabase(str, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            // Process the rows returned from the database
            rows.forEach(function(element) {
                element.forEach(function(el) {
                    if(el['col'] == 'username'){
                        if(el['val'].trim() == uname){
                            uname_match = true;
                        }
                    }
                    if(el['col'] == 'password'){
                        if(el['val'].trim() == pwrd){
                            pwrd_match = true;
                        }
                    }
                });
            });
            if(uname_match&&pwrd_match){
                //res.send('You successfully logged in!');
                ted_conn.close()
                res.send(200);
            }else{
                //res.send('Invalid credentials!');
                ted_conn.close()
                res.send(404);
            }
            
        } else {
            // No rows returns; handle appropriately
        }
    });
});

router.post('/calendar', function(req, res) {
    
    date = req.body.date;
    time = req.body.time;
    loc = req.body.location;
    desc = req.body.description;
    pro_id = generateRandomNumber(2, 1000);
    str = "insert into dbo.calendar_event (property_id, event_date, event_time, event_location, event_description) values (1,"+"'"+date+"'"+","+"'"+time+"'"+","+"'"+loc+"'"+","+"'"+desc+"'"+")";

    queryDatabase(str, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
            ted_conn.close()
            res.send(404);
        } else{
            ted_conn.close()
            res.send(200);
        }
    });
});

router.post('/register', function(req, res) {
    
    uname = req.body.username;
    pass = req.body.password;
    ten_id = generateRandomNumber(2, 1000);
    str = "insert into dbo.tenant (tenant_id, username, password) values ("+ten_id+","+"'"+uname+"'"+","+"'"+pass+"'"+")";

    queryDatabase(str, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
            ted_conn.close()
            res.send(404);
        } else{
            ted_conn.close()
            res.send(200);
        }
    });
});

router.get('/calendar/:date', function (req, res) {

    var date = req.params.date;
    str = "select event_time, event_location, event_description from dbo.calendar_event where event_date = " + date + " AND property_id = 1";

    queryDatabase(str, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            // Process the rows returned from the database
            ted_conn.close()
            res.send(rows);
            //res.send(200);

        } else {
            // No rows returns; handle appropriately
            ted_conn.close()
            res.send(404);
        }
    });
});

// router.get('/prop', function (req, res) {

//     str = "select * from dbo.tenant for json path"

//     queryDatabase(str, function(err, rows, ted_conn) {
//         if (err) {
//             // Handle the error
//         } else if (rows) {
//             // Process the rows returned from the database
//             ted_conn.close()
//             res.send(rows);
//             //res.send(200);

//         } else {
//             // No rows returns; handle appropriately
//             ted_conn.close()
//             res.send(404);
//         }
//     });
// });

router.get('/prop/:username', function (req, res) {
    var username = req.params.username;

    str = "select tenant_address, rent from tenant where username = " + username;

    queryDatabase(str, function(err, rows, ted_conn) {
        if (err) {
            // Handle the error
        } else if (rows) {
            // Process the rows returned from the database
            ted_conn.close()
            res.send(rows);
            //res.send(200);

        } else {
            // No rows returns; handle appropriately
            ted_conn.close()
            res.send(404);
        }
    });
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
